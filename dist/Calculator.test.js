// Calculator.test.ts
import "jest-extended";
import { daysBetween, afterIntervalTimes, recurringEvent, } from "./Calculator";
import { LocalDate, LocalDateTime, LocalTime, Period, } from "@js-joda/core";
describe("daysBetween", function () {
    test("should return 0 when start and end dates are the same", function () {
        var start = LocalDate.of(2022, 1, 31);
        var end = LocalDate.of(2022, 1, 31);
        expect(daysBetween(start, end)).toEqual(0);
    });
    test("should return the correct number of days between two different dates", function () {
        var start = LocalDate.of(2022, 1, 1);
        var end = LocalDate.of(2022, 1, 10);
        expect(daysBetween(start, end)).toEqual(9);
    });
});
describe("afterIntervalTimes", function () {
    test("should return the date after applying the interval and multiplier", function () {
        var start = LocalDate.of(2022, 1, 1);
        var interval = Period.ofDays(10);
        var multiplier = 3;
        var expectedDate = LocalDate.of(2022, 1, 31);
        expect(afterIntervalTimes(start, interval, multiplier)).toEqual(expectedDate);
    });
});
describe("recurringEvent", function () {
    test("should return an array of recurring dates within the given range", function () {
        var start = LocalDateTime.of(2022, 1, 1, 12, 0);
        var end = LocalDateTime.of(2022, 1, 15, 12, 0);
        var interval = Period.ofDays(3);
        var timeOfDay = LocalTime.of(15, 30);
        var expectedDates = [
            LocalDateTime.of(2022, 1, 1, 15, 30),
            LocalDateTime.of(2022, 1, 4, 15, 30),
            LocalDateTime.of(2022, 1, 7, 15, 30),
            LocalDateTime.of(2022, 1, 10, 15, 30),
            LocalDateTime.of(2022, 1, 13, 15, 30),
        ];
        expect(recurringEvent(start, end, interval, timeOfDay)).toEqual(expectedDates);
    });
    test("should return an empty array when start is after end", function () {
        var start = LocalDateTime.of(2022, 1, 15, 12, 0);
        var end = LocalDateTime.of(2022, 1, 1, 12, 0);
        var interval = Period.ofDays(3);
        var timeOfDay = LocalTime.of(15, 30);
        expect(recurringEvent(start, end, interval, timeOfDay)).toEqual([]);
    });
});
//# sourceMappingURL=Calculator.test.js.map