// Calculator.ts
// Function to calculate the number of days between two LocalDate objects.
export function daysBetween(start, end) {
    var startEpoch = start.toEpochDay();
    var endEpoch = end.toEpochDay();
    return Math.abs(endEpoch - startEpoch);
}
/* Function to calculate a date after applying the specified
interval and multiplier to a given start date. */
export function afterIntervalTimes(start, interval, multiplier) {
    return start.plus(interval.multipliedBy(multiplier));
}
/* Function to calculate an array of recurring dates between a given
 start and end LocalDateTime, with a specified interval and time of day.*/
export function recurringEvent(start, end, interval, timeOfDay) {
    var recurringDates = [];
    var currentDateTime = start;
    // Iterate until the current date-time exceeds or equals the end date-time.
    while (currentDateTime.isBefore(end) || currentDateTime.isEqual(end)) {
        recurringDates.push(currentDateTime);
        /* Increment the current date-time by the
    specified interval and set the time of day. */
        currentDateTime = currentDateTime.plus(interval);
        currentDateTime = currentDateTime.with(timeOfDay);
    }
    return recurringDates;
}
//# sourceMappingURL=Calculator.js.map