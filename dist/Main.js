/* eslint-disable @typescript-eslint/no-non-null-assertion */
/* eslint-disable no-console */
import { DateTimeParseException, LocalDate, LocalDateTime, LocalTime, Period, } from "@js-joda/core";
import { afterIntervalTimes, daysBetween, recurringEvent, } from "./Calculator";
var handleOnSubmit = function (outputID, run) { return function () {
    var output = document.getElementById(outputID);
    try {
        output.innerHTML = run();
    }
    catch (e) {
        if (e instanceof DateTimeParseException)
            output.innerText = "parse error";
        else if (e instanceof Error)
            console.error("".concat(e.constructor.name, ": ").concat(e.message));
        else
            console.error(e.toString());
    }
    return false;
}; };
var getInputElementById = function (id) {
    return document.getElementById(id);
};
var parseNonNegativeInt = function (str) {
    var val = parseInt(str);
    if (isNaN(val))
        throw new DateTimeParseException("invalid integer: " + str);
    else if (val < 0)
        throw new DateTimeParseException("expected a positive integer: " + str);
    else
        return val;
};
window.onload = function () {
    document.getElementById("daysBetween").onsubmit =
        handleOnSubmit("daysBetweenOutput", function () {
            var start = getInputElementById("daysBetweenStart");
            var end = getInputElementById("daysBetweenEnd");
            return daysBetween(LocalDate.parse(start.value), LocalDate.parse(end.value)).toString();
        });
    document.getElementById("afterIntervalTimes").onsubmit =
        handleOnSubmit("afterIntervalTimesOutput", function () {
            var start = getInputElementById("afterIntervalTimesStart");
            var interval = getInputElementById("afterIntervalTimesInterval");
            var multiplier = getInputElementById("afterIntervalTimesMultiplier");
            return afterIntervalTimes(LocalDate.parse(start.value), Period.parse(interval.value), parseNonNegativeInt(multiplier.value)).toString();
        });
    document.getElementById("recurringEvent").onsubmit =
        handleOnSubmit("recurringEventOutput", function () {
            var start = getInputElementById("recurringEventStart");
            var end = getInputElementById("recurringEventEnd");
            var interval = getInputElementById("recurringEventInterval");
            var timeOfDay = getInputElementById("recurringEventTimeOfDay");
            return recurringEvent(LocalDateTime.parse(start.value), LocalDateTime.parse(end.value), Period.parse(interval.value), LocalTime.parse(timeOfDay.value)).reduce(function (output, event) { return output + "<br>" + event.toString(); }, "");
        });
};
//# sourceMappingURL=Main.js.map