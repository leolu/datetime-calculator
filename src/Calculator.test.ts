// Calculator.test.ts
/* Portland State University
   Leo Lu
   Summer 2023
   CS 410/510
   Code Reading & Review

   Assignment 2
   Implementation
*/

import "jest-extended";

import {
  daysBetween,
  afterIntervalTimes,
  recurringEvent,
} from "./Calculator";

import {
  LocalDate,
  LocalDateTime,
  LocalTime,
  Period,
} from "@js-joda/core";

describe("daysBetween", () => {
  test("Same day should return 0", () => {
    const start: LocalDate = LocalDate.of(2022, 1, 31);
    const end: LocalDate = LocalDate.of(2022, 1, 31);
    expect(daysBetween(start, end)).toEqual(0);
  });

  test("start before end, one-day difference", () => {
    const start: LocalDate = LocalDate.of(2022, 1, 30);
    const end: LocalDate = LocalDate.of(2022, 1, 31);
    expect(daysBetween(start, end)).toEqual(1);
  });

  test("start after end, one-day difference", () => {
    const start: LocalDate = LocalDate.of(2022, 1, 31);
    const end: LocalDate = LocalDate.of(2022, 1, 30);
    expect(daysBetween(start, end)).toEqual(-1);
  });

  test("start before end, multi-month difference", () => {
    const start: LocalDate = LocalDate.of(2022, 1, 31);
    const end: LocalDate = LocalDate.of(2022, 3, 31);
    expect(daysBetween(start, end)).toEqual(59);
  });
});

describe("afterIntervalTimes", () => {
  test("Add 3 days", () => {
    const start: LocalDate = LocalDate.of(2022, 1, 31);
    const interval: Period = Period.ofDays(1);
    const multiplier: number = 3;
    expect(afterIntervalTimes(start, interval, multiplier)).toEqual(
      LocalDate.of(2022, 2, 3)
    );
  });

  test("Add 0 days", () => {
    const start: LocalDate = LocalDate.of(2022, 1, 31);
    const interval: Period = Period.ofDays(1);
    const multiplier: number = 0;
    expect(afterIntervalTimes(start, interval, multiplier)).toEqual(
      LocalDate.of(2022, 1, 31)
    );
  });

  test("Add 1 month", () => {
    const start: LocalDate = LocalDate.of(2022, 1, 31);
    const interval: Period = Period.ofMonths(1);
    const multiplier: number = 1;
    expect(afterIntervalTimes(start, interval, multiplier)).toEqual(
      LocalDate.of(2022, 2, 28)
    );
  });

  test("Add 1 year and 1 month", () => {
    const start: LocalDate = LocalDate.of(2019, 1, 31);
    const interval: Period = Period.of(1, 1, 0);
    const multiplier: number = 1;
    expect(afterIntervalTimes(start, interval, multiplier)).toEqual(
      LocalDate.of(2020, 2, 29)
    );
  });
});

describe("recurringEvent", () => {
  test("Daily recurring events from 2022-01-01 to 2022-01-04", () => {
    const start: LocalDateTime = LocalDateTime.of(2022, 1, 1, 0, 0);
    const end: LocalDateTime = LocalDateTime.of(2022, 1, 4, 23, 59);
    const interval: Period = Period.ofDays(1);
    const timeOfDay: LocalTime = LocalTime.of(1, 0);
    expect(recurringEvent(start, end, interval, timeOfDay)).toEqual([
      LocalDateTime.of(2022, 1, 1, 1, 0),
      LocalDateTime.of(2022, 1, 2, 1, 0),
      LocalDateTime.of(2022, 1, 3, 1, 0),
      LocalDateTime.of(2022, 1, 4, 1, 0),
    ]);
  });

  test("Daily recurring events from 2022-01-02 to 2022-01-04", () => {
    const start: LocalDateTime = LocalDateTime.of(2022, 1, 1, 2, 0);
    const end: LocalDateTime = LocalDateTime.of(2022, 1, 4, 23, 59);
    const interval: Period = Period.ofDays(1);
    const timeOfDay: LocalTime = LocalTime.of(1, 0);
    expect(recurringEvent(start, end, interval, timeOfDay)).toEqual([
      LocalDateTime.of(2022, 1, 2, 1, 0),
      LocalDateTime.of(2022, 1, 3, 1, 0),
      LocalDateTime.of(2022, 1, 4, 1, 0),
    ]);
  });

  test("Daily recurring events from 2022-01-01 to 2022-01-04 with end time 00:00", () => {
    const start: LocalDateTime = LocalDateTime.of(2022, 1, 1, 0, 0);
    const end: LocalDateTime = LocalDateTime.of(2022, 1, 4, 0, 0);
    const interval: Period = Period.ofDays(1);
    const timeOfDay: LocalTime = LocalTime.of(1, 0);
    expect(recurringEvent(start, end, interval, timeOfDay)).toEqual([
      LocalDateTime.of(2022, 1, 1, 1, 0),
      LocalDateTime.of(2022, 1, 2, 1, 0),
      LocalDateTime.of(2022, 1, 3, 1, 0),
    ]);
  });

  test("Monthly recurring events from 2022-01-31 to 2022-05-15", () => {
    const start: LocalDateTime = LocalDateTime.of(2022, 1, 31, 0, 0);
    const end: LocalDateTime = LocalDateTime.of(2022, 5, 15, 0, 0);
    const interval: Period = Period.ofMonths(1);
    const timeOfDay: LocalTime = LocalTime.of(1, 0);
    expect(recurringEvent(start, end, interval, timeOfDay)).toEqual([
      LocalDateTime.of(2022, 1, 31, 1, 0),
      LocalDateTime.of(2022, 2, 28, 1, 0),
      LocalDateTime.of(2022, 3, 28, 1, 0),
      LocalDateTime.of(2022, 4, 28, 1, 0),
    ]);
  });
});


