/* Portland State University
   Leo Lu
   Summer 2023
   CS 410/510
   Code Reading & Review

   Assignment 2
   Implementation
*/

import {
    ChronoUnit,
    LocalDate,
    LocalDateTime,
    LocalTime,
    Period,
} from "@js-joda/core";

/* Inputs:
   start - Start Date
   end - End Date
   returns - number
   Method returns the number of days between start and end dates.
*/
export function daysBetween(
    start: LocalDate,
    end: LocalDate,
): number {
    /* Use the until() method to calculate the number of 
    days between start and end dates.*/
    return start.until(end, ChronoUnit.DAYS);
}

/* Inputs:
   start - Start Date
   interval - Period representing an amount of time
   multiplier - Non-negative integer
   returns - LocalDate
   Method returns the date obtained by starting 
   at the start date and adding the interval period multiplier times.
*/
export function afterIntervalTimes(
    start: LocalDate,
    interval: Period,
    multiplier: number,
): LocalDate {
    let resultDate = start;

    // Add the interval to the start date 'multiplier' times.
    for (let i = 0; i < multiplier; i ++)
        resultDate = resultDate.plus(interval);
    return resultDate;
}

/* Inputs:
   start - Start DateTime
   end - End DateTime
   interval - Period representing an amount of time
   timeOfDay - Time of day
   returns - LocalDateTime[]
   Method returns an array of all DateTime instances 
   on which the recurring event should take place,
   with the event occurring once per interval at the specified timeOfDay.
*/
export function recurringEvent(
    start: LocalDateTime,
    end: LocalDateTime,
    interval: Period,
    timeOfDay: LocalTime,
): LocalDateTime[] {
    const events: LocalDateTime[] = [];
    let currentDateTime = start;

    // Iterate while the currentDateTime is before the end DateTime.
    while (currentDateTime.isBefore(end)) {
        const eventDateTime = currentDateTime.with(timeOfDay);

        // Check if the eventDateTime is not before start DateTime before adding
        if (! eventDateTime.isBefore(start))
            events.push(eventDateTime);

        // Increment the currentDateTime by the specified interval.
        currentDateTime = currentDateTime.plus(interval);
    }

    return events;
}